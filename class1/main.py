#!/usr/bin/env python
# -*- coding: utf8 -*-
from lxml import etree
import os
import re

def fileToXml(page, f):
    currentSent = ""
    while True:
        buf = f.read(1)
        if not buf: break
        if buf == "\n" or buf == "?" or buf == "!" or buf == ".":
            if currentSent == "":
                continue
            sentXml = etree.SubElement(page, 'sent')
            currentSent = re.sub(r"[$«»–()\.,%/—\-:;\n]", '', currentSent)
            sentXml.text = currentSent
            currentSent = ""
        else:
            currentSent += buf

    if currentSent != "":
        sentXml = etree.SubElement(page, 'sent')
        currentSent = re.sub(r"[$«»–()\.,%/—\-:;\n]", '', currentSent)
        sentXml.text = currentSent
    return

def textFilesToXmlFile(directory, files):
    page = etree.Element('document')
    for file in files:
        print(file)
        with open(directory + file,'r') as f:
            fileToXml(page, f)

    with open(directory + 'output.xml', 'w') as output:
        output.write(etree.tounicode(page, pretty_print=True))


directory = 'auto/'
files = os.listdir(directory)
textFilesToXmlFile(directory, files)

