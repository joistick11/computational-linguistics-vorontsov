import pymorphy2
from nltk.corpus import stopwords
from lxml import etree


def parse_xml_to_list(filePath):
    sentences = []
    tree = etree.parse(filePath)
    doc = tree.getroot()
    for node in doc:
        sentences.append(node.text)
    return sentences


def build_reversed_index(sentences):
    print("***BUILDING REVERSED INDEX***")
    index = {}
    i = 0
    for sent in sentences:
        words = sent.split(" ")
        stop_words = stopwords.words('russian')
        words = [i for i in words if (i not in stop_words and i != " ")]
        words = normalize_words(words)
        update_index(index, words, i)
        i += 1
    print("***FINISHED***")
    return index


def normalize_words(words):
    morph = pymorphy2.MorphAnalyzer()
    words_to_ret = list(map(lambda word: morph.parse(word)[0].normal_form, words))
    return words_to_ret


def update_index(index, words, i):
    for word in words:
        if index.get(word):
            index.get(word).append(i)
        else:
            index.update({word: [i]})

def search_for_words(index, words):
    results = {}
    for word in words:
        found = index.get(word)
        if found:
            if not results:
                results = set(found)
            else:
                results = results.intersection(found)
    return results