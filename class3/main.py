from class3.utils import parse_xml_to_list, normalize_words, load_reversed_index
from scipy import spatial
import operator

sentences = parse_xml_to_list("../it/output.xml")
reversed_index = load_reversed_index(sentences)

while 1:
    input_var = input("\n\nEnter a search query: ")
    search_for = normalize_words(input_var.split(" "))

    # First, create binary vector for search query and find all docs containing search words
    docs = set()  # contains docs which includes at least one word from query
    search_binary_vector = []
    for word in search_for:  # for all words in query
        # build a search vector
        if reversed_index.get(word):
            search_binary_vector.append(1)
        else:
            search_binary_vector.append(0)

        # find all doc where at least one word from query exists
        for sent_ind in reversed_index.get(word):  # put all sentences from index into set
            docs.add(sent_ind)

    # Second, create binary vectors for found docs
    docs_binary_vectors = {}
    for doc in docs:
        doc_vector = []
        for word in search_for:
            if doc in reversed_index.get(word):
                doc_vector.append(1)
            else:
                doc_vector.append(0)
        docs_binary_vectors.update({doc: doc_vector})

    # And then perform the search
    cosine_distances = {}
    for doc_id, doc_vector in docs_binary_vectors.items():
        cosine_dist = spatial.distance.cosine(doc_vector, search_binary_vector)
        cosine_distances.update({doc_id: cosine_dist})

    # Print 5 more relevant docs
    for doc in sorted(cosine_distances.items(), key=operator.itemgetter(1))[:5]:
        print(sentences[doc[0]])

