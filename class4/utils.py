import json
import os
from collections import Counter

import math
import pymorphy2
from nltk.corpus import stopwords
from lxml import etree


def parse_xml_to_list(filePath):
    sentences = []
    tree = etree.parse(filePath)
    doc = tree.getroot()
    for node in doc:
        sentences.append(node.text)
    return sentences


def normalize_words(words):
    morph = pymorphy2.MorphAnalyzer()
    words_to_ret = list(map(lambda word: morph.parse(word)[0].normal_form, words))
    return words_to_ret


def split_and_normalize_sentences(sentences):
    normalized_sentences = []
    for sent in sentences:
        words = sent.split(" ")
        stop_words = stopwords.words('russian')
        words = [i for i in words if (i not in stop_words and i != " " and i != "")]
        words = normalize_words(words)
        normalized_sentences.append(words)
    return normalized_sentences


def load_reversed_index(sentences):
    if os.path.exists('inv_ind.json'):
        print("Found saved reversed index. Loading from file...")
        with open('inv_ind.json', 'r') as fp:
            reversed_index = json.load(fp)
    else:
        print("Saved reversed index not found. Building a new one...")
        # File does not exist, create an index and save it to file
        reversed_index = build_reversed_index(sentences)
        with open('inv_ind.json', 'w') as fp:
            json.dump(reversed_index, fp, sort_keys=True, indent=4)

    return reversed_index


def get_docs_with_idf(corpus):
    def compute_idf(word, corpus):
        return math.log10(len(corpus)/sum([1.0 for i in corpus if word in i]))

    documents_list = []
    for text in corpus:
        idf_dictionary = {}
        for word in text:
            idf_dictionary[word] = compute_idf(word, corpus)
        documents_list.append(idf_dictionary)
    return documents_list


def build_reversed_index(sentences):
    print("***BUILDING REVERSED INDEX***")
    index = {}
    i = 0
    sentences = split_and_normalize_sentences(sentences) # sentences are now lists of normalized words
    sentences_with_tfidf = get_docs_with_idf(sentences)
    for sentAsDict in sentences_with_tfidf:
        update_index(index, sentAsDict, i)
        i += 1
    print("***FINISHED***")
    return index


def update_index(index, sent_as_dict, i):
    for word, tfidf in sent_as_dict.items():
        if index.get(word):
            index.get(word).update({i: tfidf})
        else:
            index.update({word: {i: tfidf}})


def search_for_words(index, words):
    docs = set()  # contains docs which include at least one word from the query
    for word in words:  # for all words in query
        if index.get(word):
            # find all docs where at least one word from query exists
            for sent_ind in list(index.get(word).keys()):  # put all sentences from index into set
                docs.add(sent_ind)

    docs_idf_sum_vector = {}
    for doc in docs:
        doc_sum = 0
        for word in words:
            if index.get(word) and index.get(word).get(doc):  # ifd value is stored in index
                doc_sum += index.get(word).get(doc)
        docs_idf_sum_vector.update({doc: doc_sum})

    return docs_idf_sum_vector