from treetagger import TreeTagger
from class7.utils import parse_xml_to_list

tt = TreeTagger(language='russian')

sentences = parse_xml_to_list("../it/output.xml")
tagged_sentences_list = [len(sentences)]
for sent in sentences:
    tagged_sentences_list.append(tt.tag(sent))

f = open('tagged_sentences.txt', 'w')
for sent in tagged_sentences_list[1:]:
    tagged_sentence = ""
    for word in sent:
        tagged_sentence += word[0] + "/" + word[1] + " "
    f.write(tagged_sentence + "\n")
