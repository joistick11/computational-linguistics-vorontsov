import pymorphy2
from lxml import etree


def parse_xml_to_list(filePath):
    sentences = []
    tree = etree.parse(filePath)
    doc = tree.getroot()
    for node in doc:
        sentences.append(node.text)
    return sentences


def normalize_words(words):
    morph = pymorphy2.MorphAnalyzer()
    words_to_ret = list(map(lambda word: morph.parse(word)[0].normal_form, words))
    return words_to_ret