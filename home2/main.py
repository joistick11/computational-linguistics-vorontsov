from home2.utils import parse_xml_to_list, normalize_words, search_for_words, load_reversed_index

# parsing xml to list of sentences
sentences = parse_xml_to_list("../it/output.xml")
# Trying to read index from file
reversed_index = load_reversed_index(sentences)

while 1:
    input_var = input("\n\nEnter a search query: ")
    search_for = normalize_words(input_var.split(" "))
    sents = search_for_words(reversed_index, search_for)

    if not sents:
        print("Nothing found")
        continue

    print("File found in {} sentences", sents)
    print("They are:")
    for i in sents:
        print(sentences[i])