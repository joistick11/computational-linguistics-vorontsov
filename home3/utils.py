import json
import os
from collections import Counter

import math
import pymorphy2
from lxml import etree
from nltk.corpus import stopwords


def parse_xml_to_list(filePath):
    sentences = []
    tree = etree.parse(filePath)
    doc = tree.getroot()
    for node in doc:
        sentences.append(node.text)
    return sentences


def build_reversed_index(sentences):
    print("***BUILDING REVERSED INDEX***")
    index = {}
    i = 0
    sentences = split_and_normalize_sentences(sentences) # sentences are now lists of normalized words
    sentences_with_tfidf = compute_tfidf(sentences)
    for sentAsDict in sentences_with_tfidf:
        update_index(index, sentAsDict, i)
        i += 1
    print("***FINISHED***")
    return index


def split_and_normalize_sentences(sentences):
    normalized_sentences = []
    for sent in sentences:
        words = sent.split(" ")
        stop_words = stopwords.words('russian')
        words = [i for i in words if (i not in stop_words and i != " " and i != "")]
        words = normalize_words(words)
        normalized_sentences.append(words)
    return normalized_sentences


def update_index(index, sent_as_dict, i):
    for word, tfidf in sent_as_dict.items():
        if index.get(word):
            index.get(word).update({i: tfidf})
        else:
            index.update({word: {i: tfidf}})


def normalize_words(words):
    morph = pymorphy2.MorphAnalyzer()
    words_to_ret = list(map(lambda word: morph.parse(word)[0].normal_form, words))
    return words_to_ret


def load_reversed_index(sentences):
    if os.path.exists('inv_ind.json'):
        print("Found saved reversed index. Loading from file...")
        with open('inv_ind.json', 'r') as fp:
            reversed_index = json.load(fp)
    else:
        print("Saved reversed index not found. Building a new one...")
        # File does not exist, create an index and save it to file
        reversed_index = build_reversed_index(sentences)
        with open('inv_ind.json', 'w') as fp:
            json.dump(reversed_index, fp, sort_keys=True, indent=4)

    return reversed_index


def compute_tfidf(corpus):
    def compute_tf(text):
        tf_text = Counter(text)
        for i in tf_text:
            tf_text[i] = tf_text[i]/float(len(tf_text))
        return tf_text

    def compute_idf(word, corpus):
        return math.log10(len(corpus)/sum([1.0 for i in corpus if word in i]))

    documents_list = []
    for text in corpus:
        tf_idf_dictionary = {}
        computed_tf = compute_tf(text)
        for word in computed_tf:
            tf_idf_dictionary[word] = computed_tf[word] * compute_idf(word, corpus)
        documents_list.append(tf_idf_dictionary)
    return documents_list
