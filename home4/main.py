import operator

from class4.utils import parse_xml_to_list, normalize_words, search_for_words, load_reversed_index

# parsing xml to list of sentences
from home4.utils import update_index_consider_relevance

sentences = parse_xml_to_list("../it/output.xml")
# Trying to read index from file
reversed_index = load_reversed_index(sentences)


while 1:
    input_var = input("\n\nEnter a search query: ")
    search_for = normalize_words(input_var.split(" "))
    docs_idf_sum_vector = search_for_words(reversed_index, search_for)

    if not docs_idf_sum_vector:
        print("Nothing found")
        continue

    found_docs = sorted(docs_idf_sum_vector.items(), key=operator.itemgetter(1))[:10]
    i = 1
    for doc in found_docs:
        print(str(i) + ". " + sentences[int(doc[0])])
        i += 1

    relevant_docs = input("\n\nPlease list the most relevant docs separated by space: ")
    # Compute new weights for words from the search query basing on the user feedback
    update_index_consider_relevance(reversed_index, search_for, relevant_docs.strip().split(" "),
                                    found_docs, len(sentences))

    print("And then update the index...")
